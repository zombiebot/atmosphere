--------------------------------------
-- Atmosphere: Initialization

-- License: MIT

-- Credits: xeranas
--------------------------------------

local modpath = minetest.get_modpath("atmosphere");
dofile(modpath.."/api.lua")

minetest.log("Atmosphere API loaded")