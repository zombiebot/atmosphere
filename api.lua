--------------------------------------
-- Atmosphere: API

-- License: MIT

-- Credits: xeranas
--------------------------------------

-- include core functions
local modpath = minetest.get_modpath("atmosphere");
local core = dofile(modpath.."/core.lua")

atmosphere = {}

atmosphere.register = function(ao)
	core.register_atmosphere(ao)
end

atmosphere.deregister = function(ao)
	core.deregister_atmosphere(ao)
end

atmosphere.is_active = function(code, pos)
	return core.is_atmosphere_active(code, pos)
end

atmosphere.activate = function(code, pos)
	core.activate_atmosphere(code, pos)
end
